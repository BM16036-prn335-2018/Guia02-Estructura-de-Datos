#include <stdio.h>

int main(){
//~ Declaracion de variables a usar en el codigo	
int fil;
int col;	

int opciones=0;

int disponible;
int posicionIngresoFila;
int posicionIngresoColumna;

int BuscarFila;
int BuscarColumna;

double ganancias=0.0;
	
	
//~ Inicializar cada una de las posiciones de la matriz a 0	
	int matriz[5][5];
	
	for(fil=0;fil<5;fil++){
			for(col=0;col<5;col++){
				matriz[fil][col]=0;
			}
		}
	
//~ Se genera el menu que vera el usuario
do{
	printf("\n==========================Menu=================================\n");
	printf("Ingrese un numero valido para acceder a las opciones del menu\n");
	printf("1-Comprar Entradas De Cine\n");
	printf("2-Buscar Asiento\n");
	printf("3-Mostrar Ganacias\n");
	printf("cualquier otro numero para salir\n");
	printf("================================================================\n");
	scanf("%d",&opciones);
	
//~ Opcion 1 compra las entradas	
	if(opciones==1){
//~ Muestra de forma ordenada las posiciones disponible en la matriz y marca con X las que no lo estan	
	printf("================================================================\n");	
		printf("\nPosiciones Disponibles Para los Asientos\n");
		for(fil=0;fil<5;fil++){
			for(col=0;col<5;col++){
				if(matriz[fil][col]==0){
					printf("(");
					printf("%d",fil+1);
					printf(",");
					printf("%d",col+1);
					printf(")");
					printf("\t");
					
					disponible=disponible+1;
				}else{
					printf("(");
					printf("X");
					printf(",");
					printf("X");
					printf(")");
					printf("\t");
				}
			}
			printf("\n");
		}
		
//If comprueba si hay una posicion disponible mediante la variable disponible si es mayor a 0 si no indica que no hay ninguna variable disponible	
		if(disponible!=0){
			
//Se compueba por medio de un Do While si la posicion se ingresa esta verdaderamente disponible y dentro de ella se comprueba que las variables que hacen referencia a las filas y columnas de la posicion que desea ingresar el asiento esten entre el margen de por cada fila y columna[1-5]	
			do{
				
			printf("\n Ingrese Una Posicion De Asiento Valida\n");
			
			do{
			printf("\n Ingrese La Fila De Su Asiento[1-5]\n");
			scanf("%d",&posicionIngresoFila);
			}while(posicionIngresoFila<1||posicionIngresoFila>5);
			
			do{
			printf("\n Ingrese La Columna De Su Asiento[1-5]\n");
			scanf("%d",&posicionIngresoColumna);
		    }while(posicionIngresoColumna<1||posicionIngresoColumna>5);
		    
		    }while(matriz[posicionIngresoFila-1][posicionIngresoColumna-1]!=0);
		    
		    
//~ Se verifica la fila para asignar el valor a las ganancias		    
		    if(posicionIngresoFila-1==0){
				ganancias=ganancias+5;
			}else if(posicionIngresoFila-1==4){
				ganancias=ganancias+2.50;
			}else {
				ganancias=ganancias+3.50;
				}
		    matriz[posicionIngresoFila-1][posicionIngresoColumna-1]=1;
		    
	printf("================================================================\n");	    
		}else {
	printf("================================================================\n");
			printf("\nNo Hay Asientos Disponibles\n");
	printf("================================================================\n");
			}
			
			disponible=0;
//~ opcion 2 Busca o comprueba si la posicion esta disponible			
	}else if(opciones==2){
	printf("================================================================\n");	
//~ se pide al usuario la posicion del asiento que desea comprobar y se verifica que esten entre el margen de filas y columnas [1-5]
		printf("\nIngrese La posicion Del Asiento Que Quiere Buscar\n");
		do{
		printf("Ingrese La Fila Del Asiento Que Quiere Buscar[1-5]\n");
		scanf("%d",&BuscarFila);
	    }while(BuscarFila<1||BuscarFila>5);
	    
	    do{
		printf("Ingrese La Columna Del Asiento Que Quiere Buscar[1-5]\n");
		scanf("%d",&BuscarColumna);
	    }while(BuscarColumna<1||BuscarColumna>5);	

//~ se comprueba si el asiento en la posicion esta disponible o no 	    	
		if(matriz[BuscarFila-1][BuscarColumna-1]==0){
			printf("El Asiento En La Fila ");
			printf("%d",BuscarFila);
			printf("y Columna ");
			printf("%d",BuscarColumna);
		    printf(" Esta Disponible\n");

			}else{
			printf("El Asiento En La Fila ");
			printf("%d",BuscarFila);
			printf("y Columna ");
			printf("%d",BuscarColumna);	
	    	printf(" No Esta Disponible\n");
			}
	printf("================================================================\n");		
	
	}else if(opciones==3){
//~ Se mustra las ganancias totales con los calculos previos
		printf("================================================================\n");
		printf("Las Ganancias Totales con : $%f",ganancias);
		printf("================================================================\n");
	}
	
}while(opciones>0&&opciones<5);

	return 0;
}
